from django.db import models
from django.utils import timezone
from datetime import datetime, date

class Matkul(models.Model):
    nama_matkul = models.CharField(max_length=100)
    dosen = models.CharField(max_length=100)
    sks = models.CharField(max_length=30)
    deskripsi = models.CharField(max_length=255)
    semester = models.CharField(max_length=30)
